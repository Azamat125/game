import { Router } from "express";
import generalRoute from "./generalRoute.ts/generalRoute";

const mainRoute = Router();

mainRoute.use("/", generalRoute)

export default mainRoute;
