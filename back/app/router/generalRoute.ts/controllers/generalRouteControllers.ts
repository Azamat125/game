import * as express from "express";

let randomNumber;
const min = 1000;
const max = 9999;

const generalRouteControllers = {
    async getRandomNumber(req: express.Request, res: express.Response) {
        randomNumber = Math.floor(Math.random() * (max - min + 1)) + min
        console.log(randomNumber)
        try {
            return res.send({ message:randomNumber })
        }
        catch (e) {
            return res.status(400).send(e)
        }
    },
    async checkNumber(req:express.Request,res:express.Response){
        const guesNumber = req.body.message.toString();
        const computerNumber = randomNumber.toString()
        var bulls = 0;
        var cows = 0;
        try{
           for(let i = 0;i<guesNumber.length;i++) {
               if(guesNumber[i] === computerNumber[i]) {
                   bulls++;
               } 
                else if(computerNumber.indexOf(guesNumber[i])>=0){
                    cows++
                }
           }
            return res.send({message:{bulls,cows}})
        }
        catch(e){
            return res.status(400).send(e)
        }

    }
}

export default generalRouteControllers;