import { Router } from "express";
import generalRouteControllers from "./controllers/generalRouteControllers";

const generalRoute = Router();

generalRoute.get("/randomNumber", generalRouteControllers.getRandomNumber)
generalRoute.post("/checkNumber", generalRouteControllers.checkNumber)

export default generalRoute;