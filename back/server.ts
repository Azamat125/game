import mainRoute from "./router/mainRouter";
import express from "express";
import cors from "cors";
import * as dotenv from "dotenv"
dotenv.config({path:"../.env"})

const app = express();
const port = process.env.REACT_APP_BACK_PORT;
const corsOptions = {
    origin:`http://${process.env.REACT_APP_HOST_NAME}:${process.env.REACT_APP_FRONT_PORT}`,
    optionSuccessStatus:200,
}
console.log(process.env.REACT_APP_HOST_NAME);
console.log(process.env.REACT_APP_FRONT_PORT);

app.use(cors(corsOptions))
app.use(express.json())
app.use(express.static(__dirname))

app.use(mainRoute)




const start = () => {
    try {
        app.listen(port, () => {
            console.log("server started on port: ", port)
        })
    }
    catch (e) {
        console.log(e)
    }
}

start();
