import {UserTestInterface} from "../../assets/interfaces";
import "./userTest.css";


const UserTest:React.FC<UserTestInterface> = ({data}) =>{
    console.log(data)
    return(
        <div className="answers__container">
            <div>Быков {data.bulls}</div>
            <div>Коров {data.cows}</div>
        </div>
    )
}

export default UserTest