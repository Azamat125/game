import axios from "axios";

console.log(process.env.REACT_APP_HOST_NAME);
console.log(process.env.REACT_APP_BACK_PORT);

export const instance = axios.create({
  baseURL: `http://${process.env.REACT_APP_HOST_NAME}:${process.env.REACT_APP_BACK_PORT}`,
});
