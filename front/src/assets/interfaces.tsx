export interface CurrentNumber {
    value:string;
}
export interface HistoryGues {
    bulls:string;
    cows:string;
}
export interface HistoryState {
    list:HistoryGues[]
}
export interface UserTestInterface {
    data:HistoryGues
}