import { useEffect, useState } from "react";
import "./App.css";
import {instance} from "../assets/assets"
import UserTest from "../components/userTest/userTest";
import {CurrentNumber,HistoryState} from "../assets/interfaces"
import advice from "../images/lightbulb.svg";

const App = ()=> {

  const [currentNum,setCurrentNum] = useState<CurrentNumber>({value:""});
  const [history,setHistory] = useState<HistoryState>({list:[]})
  const [validate,setValidate] = useState(false)

  useEffect(()=>{
    getRandomNuber()
  },[])

  const getRandomNuber = async()=>{
    const response = await  instance.get("/randomNumber");
    console.log(response)
  }

  const changeHandler = (e:React.ChangeEvent<HTMLInputElement>)=>{
    if(e.target.value.length>4){
      setValidate(true)
    }
    else {
      setValidate(false)
      setCurrentNum({value:e.target.value})
    }

  }

  const checkNumber = async ()=>{
    if(!validate && currentNum.value.length === 4) {
      const response = await  instance.post("/checkNumber",{message:currentNum.value});
      setHistory(prevState=>({
        list:[...prevState.list,response.data.message]
      }))
    }
    return 
  }

    
  console.log(history)
  return (
    <div className="container">
        <div className="game__wrapper">
            <div className="game__items">
                <h2>Игрок</h2>
                {history.list.map((el,index)=>(<UserTest data={el} key={index}/>))}
            </div>
            <div className="game__items game__df">
                <input className="game__input" placeholder="Введите число " type="number" value={currentNum.value} onChange={(e)=>changeHandler(e)}/>
                {validate&&<span className="game_validate">Число не должно превышать 4 символов</span>}
                <button className="game__button" onClick={checkNumber}>Проверить</button>
            </div>
            <div className="game__items game__df">
                <h2>Компьютер</h2>
                <img alt="advice" src={advice} className="game__icon"/>
                <span className="game__advice">
                  Компьютер загадал четырехзначное число
                </span>
                <span className="game__advice">
                  Игрок, сделай ход, чтобы узнать это число 
                </span>
                <span className="game__advice">
                 Компьютер в ответ показывает число быков(число отгаданных цифр, стоящих на своих местах) и число коров(число отгаданыых цифр, стоящих не на своих местах)
                </span>
            </div>
        </div>
    </div>
  );
}

export default App;
